# goutils

[![License](https://img.shields.io/gitlab/license/seppiko/goutils?style=flat-square)](LICENSE)
![GitLab tag (latest by SemVer)](https://img.shields.io/gitlab/v/tag/seppiko/goutils?style=flat-square)

A golang toolkit for restrained and streamlined
