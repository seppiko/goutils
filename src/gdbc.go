/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
	"database/sql"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

/*
 * mysql://host:port/databaseName
 * postgres://host:port/database[?parameter_list]
 */
func GetGdbc(gdbcUrl string, username string, password string) (*sql.DB, error) {
	u, err := url.Parse(gdbcUrl)
	if err != nil {
		return nil, err
	}

	var sqlUrl string
	if u.Scheme == "mysql" {
		h := strings.Split(u.Host, ":")
		hostname := h[0]
		port, _ := strconv.ParseInt(h[1], 0, 64)
		database := u.Path[1:]
		sqlUrl = fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", username, password, hostname, port, database)
	} else {
		sqlUrl = gdbcUrl
	}

	return sql.Open(u.Scheme, sqlUrl)
}
