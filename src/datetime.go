/*
 * Copyright 2023 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package lib

import (
	"strings"
	"time"
)

var (
	longDayNames = []string{
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
	}

	shortDayNames = []string{
		"Sun",
		"Mon",
		"Tue",
		"Wed",
		"Thu",
		"Fri",
		"Sat",
	}

	shortMonthNames = []string{
		"Jan",
		"Feb",
		"Mar",
		"Apr",
		"May",
		"Jun",
		"Jul",
		"Aug",
		"Sep",
		"Oct",
		"Nov",
		"Dec",
	}

	longMonthNames = []string{
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	}
)

/*
 * yyyy-MM-dd hh:mm:ss.SSS to 2006-01-02 15:04:05.000
 * yyyy-MM-dd HH:mm:ss.SSS to 2006-01-02 03:04:05.000PM
 */
func _convertFormat(datetimeFormat string) string {
	goDatetimeFormat := datetimeFormat
	goDatetimeFormat = strings.Replace(goDatetimeFormat, "yyyy", "2006", 1)
	goDatetimeFormat = strings.Replace(goDatetimeFormat, "yy", "06", 1)
	goDatetimeFormat = strings.Replace(goDatetimeFormat, "MM", "01", 1)
	goDatetimeFormat = strings.Replace(goDatetimeFormat, "dd", "02", 1)
	if strings.Contains(goDatetimeFormat, "HH") {
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "HH", "15", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "mm", "04", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "ss", "05", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "SSS", "000", -1)
	} else if strings.Contains(goDatetimeFormat, "hh") {
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "hh", "03", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "mm", "04", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "ss", "05", 1)
		goDatetimeFormat = strings.Replace(goDatetimeFormat, "SSS", "000", -1)
		goDatetimeFormat = goDatetimeFormat + "PM"
	}

	return goDatetimeFormat
}

func format(time time.Time, format string) string {
	return time.Format(_convertFormat(format))
}
